//1. using the new keyword

// const books  = new Array('css','html','node.js')
// console.log(books)

//2. Accessing Arrays
// const books = ['mongodb','node Js','React Js']
// const b1 = books[0]
// console.log(b1)

//3. Iterating over an array

// const books = ['mongodb','node Js','React Js','Angular','css','HTML']

// for(i=0;i<books.length;i++){
//     const array = books[i]
//     console.log(array,(i))
// }

//4. Array Methods
//const books = ['mongodb','node Js','React Js','Angular','css','HTML']
//push()
// books.push('Python') // Add new to end

//pop()
// books.pop() //Remove Last

//unshift()
// books.unshift('novel') // Add first

//shift()
// books.shift() // remove first element

//indexOf()
//  const result = books.IndexOf("css") // show index of num

//lastIndexOf()
// const results = books.lastIndexOf("css")

//include()
//console.log(books.includes("express.js"))

//reverse()
// console.log(books.reverse())

//============================================================

// reduce ,filter , find

//----REDUCE----

//reduce return a single value
//does not mutate

// const product = [12, 30, 100];

// array.reduce(function(acc,currValue,currIndex,arr){} initial)

// // Find the total qty in the array

// const ans = product.reduce(function (acc, currVal, idx, arr) {
//    return acc + currVal;
// }, 0);
// console.log(ans);

//----FIND-----

// const agesArr = [2,5,6,20, 30, 40, 50, 24];

//syntax

//array.find(function(data){

//})

//find age less than 30 Find return single value

// const result = agesArr.find(function (age) {
//   return age < 30;
// });
// console.log(result)

//----Filter----
// const agesArr = [2,5,6,20, 30, 40, 50, 24];

// const result = agesArr.filter((item)=>{
//     return item > 30
// })

// console.log(result)

//*************************************************************************

// const str = ['react.js','javascript','node.js','express.js']
// for(i=0;i<str.length;i++){
//     console.log(`This is string num`,i)
// }
//const arr = ['react.js','javascript','node.js','express.js']

//----------------------------------------------------------------------------
-
//Reverser Array
// const ReverseArr = function (arr) {
//   return arr.reverse();
// };
// const arrData = ['react.js','javascript','node.js','express.js']
// const result = ReverseArr(arrData)
// console.log(result)

//--------------------------------------------------------------------------

//#Sum all num of array
// const sum = [25,6,9,2,1]

// const SumAllNummFun = function(arr){
// const total = arr.reduce(function(acc,currvalu){
//     return acc + currvalu;
// },0)
// return  total;
// }
// const ans2 = SumAllNummFun(sum)
// console.log(ans2)

//----------------------------------------------------------------------------

//#
// const avgNum = function (arr) {
//   const avg = arr.reduce(function (acc, currV) {
//     return acc + currV/arr.length
//   }, 0);
//   return`$ ${ avg.toFixed(2)}`
// };

// const ans = avgNum([2,3,4,5,9,4]);
// console.log(ans)

//----------------------------------------------------------------------------

//#RemoveDupalicate Array
// const arrData = [2, 4, 6, 2, 4, 4, 5, 8, 9, 4, 5, 9, 7];

// function removeDuplicates(arr) {
//   const duplicate = arr.filter(function (el, idx, arr) {
//     // console.log("element",el)
//     // console.log("index",idx)
//     // console.log("array",arr)
//     return arr.indexOf(el) === idx;
//   });
//   return duplicate;
// }
// removeDuplicates(arrData);
// const res = removeDuplicates(arrData);

// console.log(res);
// console.log(arrData);

//----------------------------------------------------------------------------
/*write a function that takes array of words and return a 
new Array with only the words that are three letters or shorter */

//const arrData = ["react.js", "javascript", "node.js", "express.js", "Css"];

// const FindThreeLetter = function (arr) {
//   return arr.filter(function (el,idxs, arr) {
//     return el.length <= 3;
//   });
// };
// const resu = FindThreeLetter(arrData);
// console.log(resu);


//----------------------------------------------------------------------------

//#Arrow function method
// const FindThreeLetter = (arr) => {
//   return arr.filter((item) => {
//     return item.length <= 3;
//   });
// };

// const res = FindThreeLetter(arrData);
// console.log(res);

//----------------------------------------------------------------------------
