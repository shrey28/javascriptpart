// Object Constructor functon

// const person = new Object()

//add properties

// person.name = "Shrey"
// person.age = 26
// person.isMarried = false
// person.hobbies = ["Sport","Cooking"]

// const person = new Object({
//     name:'shrey',
//     age:26,
//     hobbies:["Sport","Cooking"],
//     isMarried : false
// })

//#Object literal syntax

// const person = {};

//add Properties

// person.name="shrey",
// person.age= 30,
// person.isMarried = false
// person.hobbies = ["Sport","Cooking"]

// //Change Array in Object
// const person = {
//   name: "shrey",
//   age: 26,
//   hobbies: ["Sport", "Cooking"],
//   isMarried: false,
// };
// // update Properties
// person["age"] = 90;

// // update Properties
// person["name"]= "Ashwary"

// // update Properties
// person.hobbies = ["Programming", "Coding"];

// // Delete properTies
// delete person.age;

// console.log(person);

// const person = {
//   fname: "shrey",
//   lname: "pathak",
//   age: 26,
//   viewsCount: 100,
//   hobbies: ["Sport", "Cooking"],
//   isAdmin: true, //i wont deleted Admin Properties
//   isMarried: false,
//   status: false,
//   printFullName: function () {
//     console.log(`My Fullname is ${this.fname} ${this.lname} `);
//   },
//   checkStatus: function () {
//     if (this.status === false) {
//       delete this.isAdmin;
//     }
//   },
// };
// person.checkStatus();

// Working this Function
// person.perintFullName = function(){

//     console.log(`My Fullname is ${this.fname} ${this.lname} `)
// }
// person.perintFullName()

// Normal Use
// const fullName = `${person.fname} ${person.lname}`
// console.log(fullName)

// const obj = {
//   fname: "shrey",
//   lname: "pathak",
//   age: 26,
//   viewsCount: 100,
//   hobbies: ["Sport", "Cooking"],
//   isAdmin: true, //i wont deleted Admin Properties
//   isMarried: false,
//   status: false,
// };

//1. for in loop
//2. object.keys
//3. object.values
//4. object.entries

//1. for in loop
// for(let key in obj){
//    console.log(key)
// }

//----------
//2. object.keys
// const mykeys = Object.keys(obj)
// console.log(mykeys)

// // const arr = [1,4,6,2,8];

// mykeys.forEach(function(data){
//     console.log(data)
// })
//--------------
//3. object.values
// const ObjValues = Object.values(obj)
// console.log(ObjValues)
//-------------
//4. object.entries
// const ObjEntries = Object.entries(obj);
// ObjEntries.forEach(function(entry){
//     console.log("ForEach",entry)
// })

//#Destructuring Assignment
// const ObjEntries = Object.entries(obj);
// ObjEntries.forEach(function([key,value]){
//     console.log(`${key} ${value}`)
// })

// console.log(ObjEntries);

//=======================
///////prectice////////

// const user = {
//   name: "shrey",
//   age: 26,
//   location: "indore",
//   printInfo: function () {
//     return `${this.name} is ${this.age} Year old`;
//   },
// };
// const ans = user.printInfo();
// console.log(ans);

//+++++++++++++++++++++++++++++++++++++

// const user = {
//   name: "shrey",
//   age: 26,
//   location: "indore",
//   married: false,
// };
// user.isMarried = function () {
//   if (this.married) {
//     console.log(`${this.name} is married`);
//   } else {
//     console.log(`${this.name} is not married`);
//   }
// };

// //invoke function()

// user.isMarried();

//+++++++++++++++++++++++++++++++++++++++++

//write a function that iterates over an object print out each
//key value pair in format "key: value" on separate lines
// const user = {
//   name: "shrey",
//   age: 26,
//   location: "indore",
//   married: false,
// };

// const keysObj = function (obj) {
//   for (let key in obj) {
//     console.log(obj[key]);
//   }
// };
// keysObj(user)
