//function Scope varibale and functon visibilites

//#Global Scope

// let user = {
//     name:"Sufi",
//     age:24
// }

// function NewUser(objectUser){ // send object this parameter
// console.log(`My name is ${objectUser.name} and i ${objectUser.age} year old`)
// }

// //NewUser(user)//get object

// //update user

// function changeUserInfo(obj){
// obj.name="Shey";
// obj.age=26;
// }
// changeUserInfo(user)

// NewUser(user)//get object

//#Block Scope

// let age = 30;
// if(age>18){
//     let msg = "Your old enough"
//     console.log(msg) //Block Scope
// }
// console.log(age)//global scope

//Function scope

// const myfun =()=>{
//    // console.log(x)// is not dec
//     let x =  "Hello";
//     console.log(x)
// }
// console.log(x)//not global scope
// myfun()

//Lexical scope

// function myfun() {
//   let x = "Hello";
//   console.log(x);
//   function anotherfun() {
//     let y = 10;
//     console.log(y);
//   }
//   //invoke
//   anotherfun()
// }
// myfun();

// const Duplicte = [1, 2, 2, 3, 4, 5, 8, 7, 4, 2, 5, 5, 5, 2, 2];
// const Duplicte2 = [5, 4, 5, 8, 6, 5, 4, 4, 42, 2, 25, 4, 52, 3];

// const RemoveDuplicateArr = function (arr) {
//   const NewDulicteList = arr.filter(function (el, idx, arr) {
//     return arr.indexOf(el) === idx;
//   });
//   const anotherfun = function (arr) {
//     const MaxValue = arr.filter((item) => {
//       return item > 42;
//     });
//     return MaxValue;
//   };
//   const NewArray = anotherfun([...Duplicte, ...Duplicte2]);

//   console.log(NewArray);
//   return NewDulicteList;
// };

// const result = RemoveDuplicateArr([...Duplicte, ...Duplicte2]);

// console.log(result);

// const CountDup = function(arr){
//   return arr.reduce(function(acc,currvalu){
//      acc[currvalu]= ++acc[currvalu]||1
//            return acc;
//    },{})

//  }

//  const result = CountDup(arr)
//  console.log(result)

// const myArray = [false, 24, "English", false, "english", 22, 19, false, "English", 19];

// const itemCounter = (array, item) => {
//     return array.filter((currentItem) => currentItem == item).length;
// };

// console.log(itemCounter(myArray, 19)); // 2

//High Order funtion as first class citizen in javascript

// function addTwoNums(){
//   return function(a,b){
//     return a + b
//   }
// }
//#invoke
// const ans = addTwoNums()
// console.log(ans(2,5))

//#another way of calling this fn

// const ans = addTwoNums()(2,2)
// console.log(ans)

//assign fn to  a varible
// function sayHeloo(){
//   console.log("How are you")
// }

// const result = sayHeloo
// result()
// sayHeloo()

//#High Order funtion
//Hight order function simply implements that
//functon acceptin function as a segment and is als
// reduce() find() filter() map()

// const ayt =[1,4,5,6,8];
// ayt.find(function(data){
// console.log(data)
// })

//#High Order funtion
//Calc Bill
// const calcBill = function(qyt,price){
//   return qyt * price
// }

// //Display Bill
// const displayBill = function(calcBill){
// console.log(calcBill)
// }

// displayBill(calcBill(2,5))

//#Function retruning another function

// function addTwoNums(){
//   return function(){
//     console.log("Am called")
//   }
// }

// const firstFun =  addTwoNums()
// const secondFn = firstFun()
// console.log(secondFn)

// addTwoNums()();

// function addTwoNums(a) {
//   return function (b) {
//     return function (c) {
//       return a + b + c;
//     };
//   };
// }
// // const firstFun = addTwoNums(2);

// // const secondFn = firstFun(3);

// // console.log(secondFn);

// const firstFun = addTwoNums(2)(3)(4);
// console.log(firstFun);

//#closer Function()

// function logHello(){
//   console.log("Hello I'm Closer function");
// }

// (function sayHello() {
//   logHello()
// })();

// use Arrow function with closer 
// (()=>console.log("Hello Closer"))()

// const hq = document.querySelector("#js")
// hq.style.color="red"
// hq.style.padding="20px"