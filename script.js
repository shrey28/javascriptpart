//Varibles

//keywords Used to Javascript
//var If you want your code to run in older browsers
//let  variables that are limited to the scope of a block statement
//const = Constant value is a fixed value. is not reAssign

// age is a varible and 20 is age value
//  var age = 100
//  var name = "shrey"

// declar multiple variables
// var age = 20,
//   ctiy = "indore",
//   isMarried = false;

//# declare a varible without a value

//   let country; //undefined

// ----------------------------------------------

// # Rules of Javascript
// 1 case-sensitive
// 2 statemet must be end with semincolon
// white spaces are igonored
// Comments are  ignored
// types of comments  sigleline comments // and multiline /**/

// let myvariable = 'anything';
// console.log(myvariable)

//#case-sensitive
// let  name = "shrey"
// Name = "pathak"

// console.log(name,Name)

// Data Types Javascript

//# Primitive values , undefined ,Numeber ,string ,boolean, Null, symbole

// let num1 = 2;
// let num2 = 2;
// let sum = num1 / num2;
// console.log(sum);

// let modResults = 3 % 2
// console.log(modResults)

//------------------------------------------------------

// increment (++) decrement (--)
// Postfix after
// prefix before

// Postfix after
// let x = 3;
// let y = ++x ;
// console.log(x)

// prefix before
// let a =  6
// let b = a--
// console.log(b)

// ----------------------------------------------------
//# Assignment opeator

// = , += , -=, /=

// let x = 2
// x += 20
// console.log(x)

// #Difference between = , == , ===
// == only compare value 5 == '5' true
// ===  compare value and Type 5 === '5' false

// #Comparison Operatores
// greaer then  > less then <   (badha) >= (chhota) <=

//#LOGICAL Operatores
// and && or || not !

//if both condition && true
//if one condition true ||
//if is not equal  between to value

// let fullname = undefined
// let age  = 26;
// let isMarried =  true

// let result1 = fullname && age
// let result2 = fullname || age
// let result3 = !isMarried

//---------------------------------------------------

//# Conditional statments

//  if('conditon'){
//     //code to run
//   }

// let age = 18

// if(10 > age){
// console.log('Yes you qualified to watch moives')
// }else{
//     console.log('Go home')
// }

// let age = prompt("Enter your age")
// if (age >= 18 && age < 60 ) {
//     console.log('You are allowed to watch this movie');
//   } else if (age >= 60) {
//     console.log('You are too old to watch this movie');
//   } else {
//     console.log('You are not allowed to watch this movie');
//   }

//----------------------------------------

//# Loops
// for, while , do While
// for(initialization ; condition ; increment){
//     code to run
// }

// for(i=1;i<=5;++i){
//     console.log(i)
// }

// let i = 0
// while(i<=5){
//     console.log(i)
//     i += 1
// }

// #odd and even
// for(i=10;i<=100;i++){
//     if(i%2==0){
//         console.log(i)
//     }
// }

// for (let i = 8; i < 100; i++) {
//     let x = i + 2;
//     console.log(x);
//     i++;
//   }
//----------------------------------------------------

//#Function

// function function_name(params) {
//     //code to be executed
// }

// function sayHello(){
//     console.log("Hello welcome to function")
// }
// sayHello()

//function expression

// let greeting = function(){
//     console.log("Good morning")
// }
// greeting()

// let addNum = function(){
//     let x = 20
//     let y = 30
//     let result = x + y
//     console.log(result)
// }
// addNum()

// let addNum = function (x, y) {
//   let result = x + y;
//   console.log(result)
// };
// addNum(20, 50);

//#The return keyword is used to exit a function and return a value to the caller.
// let addNum = function (x, y) {
//   return x + y;
// };
// let ans = addNum(20, 50);
// console.log(ans);

//----------------------------------------------------------

// # String
// # ways of creating string

//const myName = "Shrey";

//# using the constructor function
//#creating an instance of string but A1 is advanced topics

//const firstName = new String("Shrey");// #always create an instance of a string out og an object
//deff between
// console.log(myName, firstName);
// const result = myName == firstName
// console.log(typeof firstName) // object

//---------------------------------------------------------------
//#String Concatenation
// + operator

// const str1 = "Hello"
// const str2 = "How are you"

// const result = str1 +" "+ str2
// console.log(result)

//# How can be concat string

// const result = str1.concat(" "+ str2)
// console.log(result)

//------------------------------------------------------------------

//#Template literals `` lot of flexiblities

// const msg = `Good day brother`

// const msg1 = `"Good evening"`

// console.log(msg,msg1)

//#mutiline string

// const msg =
// `
// welcome to our appliction try
// to make sure your have
// an
// acoount
// `;

// console.log(msg)

//#interpolation
// const age = 30;
// const welcome = `This is your age ${age}`;
// const welcome = "This is your age" + age;
// console.log(welcome);

//# String length

// const lastName = "Shrey ";
// const result =  lastName.length  //this is not method is propty

// console.log(result)

// const result = isLongerThan("Shrey", 4);
// console.log(result)

//#string indexof

// const str = "Coding time"
// const res = str.indexOf("C")
// console.log(res)

//------------------------------------------------------

// function stringlength(str) {
//   return str.length;
// }

// //invokr/call
// const result = stringlength("Shrey Pathak");

// console.log(result)

// const isLongerThan = function (str, num) {
//   if (str.length > num) {
//     return true;
//   } else {
//     return false;
//   }
// };

//------------------------------------------------------
/*c
create a function to check if astring contains whitespace

The function should take one argument the string

The function should return true if the  string 
contains whites and false if it doen not */

// function whitespaceAgrumnt(str){
// return str.indexOf(" ") !== -1
// }
// const res = whitespaceAgrumnt("shrey")
// console.log(res)

// string spilt method is convert to  array

// const str = "shrey"
// const res = str.split("") // use ' '  is seprated with this comma ,

//#join()

// const convertedStr = res.join("") // by default join seprated
// console.log(convertedStr)

//#revers

// const convertedStr = res.reverse()
// console.log(convertedStr)

//#slice

//  const convertedStr = res.slice() //convert array
//  console.log(convertedStr)

//challenge 

// const seperateString = function (arr, char) {
//     return arr.join(char);
// };
// const result = seperateString(['js','css','node js'],"_")
// console.log(result)

// function reverString(str){
//     return str.split('').reverse().join("")
// }
// const result = reverString('Node.js')
// console.log(result)

//Without Function 

// const str = "Node.js"
// res = str.split('').reverse().join('')
// console.log(res)

// Explation function 

// function reverseString(str){
//     //cover string to array
//     let strArr = str.split()
//     console.log(strArr)

//     // reversed the converted string 

//     let reverseArr = strArr.reverse();
//     console.log(reverseArr)

//     // to join string
//      return reverseArr.join("")
// }
// const result  = reverseString("node,js")
// console.group(result)

//--------------------------------------------------------------------


