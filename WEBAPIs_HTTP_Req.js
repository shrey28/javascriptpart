//Types of API Browser And Third Party

const batteryLevel = document.querySelector(".batteryLevel");
const batteryCharging = document.querySelector(".batteryCharging");
const batteryChargingTime = document.querySelector(".batteryChargingTime");
const batteryDisChargingTime = document.querySelector(
  ".batteryDisChargingTime"
);

//Batter API //in is return for true

const battery = () => {
  if ("getBattery" in navigator) {
    navigator.getBattery().then((battery) => {
      function updateAllBatteryDetails() {
        updateCharginginfo();
        updateLevelChange();
        updateDisChargeInfo();
        updateChargingTimeChangeInfo();
      }
      updateAllBatteryDetails();
      //#Battery Charging change
      battery.addEventListener("chargingchange", () => {
        updateCharginginfo();
      });
      function updateCharginginfo() {
        const isCharging = battery.charging ? "Yes" : "no";

        batteryCharging.innerHTML = isCharging;
      }
      //#Battery charging time
      battery.addEventListener("chargingTimeChange", () => {
        updateChargingTimeChangeInfo();
      });

      function updateChargingTimeChangeInfo() {
        console.log(battery.chargingTime);
        batteryChargingTime.innerHTML = battery.chargingTime;
      }

      //Battery Discharge time
      battery.addEventListener("DischargingTimeChange", () => {
        console.log("DischargingTimeChange");
      });
      function updateDisChargeInfo() {
        batteryDisChargingTime.innerHTML = battery.dischargingTime + " Sec";
      }

      //Battery Level
      battery.addEventListener("levelChange", () => {
        updateLevelChange();
      });

      function updateLevelChange() {
        const level = battery.level * 100 + "%";
        batteryLevel.innerHTML = level;
      }
      //Battery status
    });
  }
};
battery();
