//ES6

//#Array Destructure

// const users =[
//     {username:"Ben",age:28},
//     {username:"John",age:30}
// ]
// const [user1,user2]=users
// console.log(user1)
// console.log(user2)

//#object Destructure

// const obj = {
//     firstName:"John",
//     age:24
// }

// const {firstName:myName,age:myAge} = obj

//#add defulat Values

// const obj = {
//     firstName:undefined,
//     age:24
// }

// const {firstName='Ben',age,city="NewYork"} = obj

//#Destructure Parameters
// const userObj = {
//   firstName:"John",
//   age: 24,
// };

// getUser(userObj)

// function getUser(user){
//     console.log("First name",user.firstName)
//     console.log("year old",user.age)
// }

//#Rest Parameters

// function addTowNums(a,b,...rest){
//     console.log(a,b)
//     console.log(rest)//put on array
// }
// addTowNums(20,30,10,"S") //

// function findMax(...rest){
//     console.log(Math.max(...rest))
// }
// findMax(10,20,30,100)

//#Arrow Function

// const add2 = a => a;
// console.log(add2(2))

// const add3 = (a,b)=>{
//     return a+b;
// }
// const sumTowNums = add3(2,5)
// console.log(sumTowNums)

//#Spread Operator
/*The spread operator (...) is a convernient way to 
copy all  or part of an  
existing array or object into anther array or object
*/

/* Spread Vs Rest Spread syntex "enpands" an array
into its elements while
rest syntax collects multple
element and "condenses" them into a single element 
*/

//Spread operator(...) === expand

// function multiply(...rest){
//     console.log(rest)
// }
// multiply(1,3)

//spread strings
// console.log(..."welcome");

//spread arrays
// console.log([..."welcome"]);

//Combine arrays
const arr = [1, 2, 3, 4, 5];
const arr2 = [6, 7, 8, 9, 10];

//traditional
// const newArr = arr.concat(arr2)
// console.log(newArr)

//modern
// const newArr = [...arr,...arr2]
// console.log(newArr)

//mutate
//const originalArr = ["Ben", "Joe", "Kimi"];

//Copy the original array
// const NewArray = [...originalArr];
// console.log("Before pushing",originalArr)

// //add new user

// NewArray.push("Joe");

// console.log("Before pushing",NewArray)

// const obj1 = {
//   name: "John",
//   age: 30,
// };
// const obj2 = {
//   city: "NY",
//   country: "USA",
// };

// //Traditional
// const newObj = Object.assign({}, obj1, obj2);

// //ES5

// const newObj2 = { ...obj1, ...obj2 };
