//Synchronous Programming one by one serve
//Asynchronous Programming and this order ready is serve

//#Synchronous Programming

//setTimeout
// const msg =()=>{
//     console.log("Hello Your order is ready")
// }

// setTimeout(msg,3000)

// setTimeout(()=>{
// msg()
// },3000)

//#Asynchronous Programming

// const msg = () => {
//   console.log("How are You");
// };
// console.log("Go to park");

// setTimeout(msg, 3000);

// console.log("Go to Bank");

//Async code writing way
//*setTimeout,*callback,*promise, *Aysnc/Await

/*callback why we need callback function()
This a function that is prassed to another function as an
argument this function then execute oftenn the functon
that is passed to is executed

function sayHello(callback){
    console.log("Hello");
    callback();
}
*/

//we pass argument to this function callback()
//login create a post and fetch all post

// const postData = [
//   {
//     title: "Title 1",
//     desc: "Desc 1",
//   },
//   { title: "Title 2", desc: "Desc 2" },
//   {
//     title: "Title 3",
//     desc: "Desc 3",
//   },
// ];

// function fetchPosts(){
//     console.log("Post is Fatching....")
//     console.log(postData)
// }
// fetchPosts()

// //create post
// function  createPost(post,cb){
//     //push the postData
//     setTimeout(()=>{
//         postData.push(post)
//         //invoke callback
//         cb()
//     },2000)

// }
// //invoke
// createPost({
//     title:"Title 4",
//     desc:"Desc 4",
// },

// fetchPosts

// )

//#Promise
/* 
when a promise is created it in the pending
state 
when a promise is resolve it is in the 
fulfilled state
when a promise is reject it is in the 
rejected state
*/

// const userPromise = new Promise((resolve, reject) => {
//   //code
//   let user = {
//     name: "Shrey",
//     city: "Indore",
//   };
//   let isFetched = true;

//   if (isFetched) {
//     resolve(user,"Yes it is resolve");
//   } else {
//     reject('error fectching user .try agian later');
//   }
// });

// //Resolving error or success
// userPromise.then(function(valueFromResolve){
//   //get the values from resolve fn .then()
//   console.log(valueFromResolve);
// }).catch((err)=>{
//     console.log('Something went wrong',err);
// })

//Function Returning a promise

// function getUser() {
//   new Promise((resolve, reject) => {
//     let user = {
//       name: "Shrey",
//       city: "Indore",
//     };
//XXXXXXXXXXXXX
//     let isFetched = true;
//     if (isFetched) {
//       resolve(user);
//     } else {
//       reject();
//     }
//   });
// }

// getUser().then(function (res) {
//   console.log(res);
// });
// ==================================================

//consumming multiple promise
//fatch all comment associated with a post
//step 1 promise.all()
//step promise.race()

// const commentPromise = new Promise(function(resolve,reject){
//     let commentFatched = true;
//     let comment={
//         title: "Title 1",
//         //     desc: "Desc 1",
//     }
//     if(commentFatched){
//         resolve(comment)
//     }else{
//         reject("Error fetching comment")
//     }
// })

// //resolve multi promise
// Promise.all([commentPromise]).then((allPromise)=>{
//     console.log(allPromise);
// })
//================================================

//Async/Await use amount of promises and future
/* 
async function fetchPosts(){
    const res = await makeAPIResquest()
} 
*/

//Async/Await
// function makeAPIResquest() {
//   return new Promise((resolve, reject) => {
//     setTimeout(() => {
//       let isFetched = false;
//       let user = {
//         name: "Miki",
//         age: 20,
//       };
//       if (isFetched) {
//         resolve(user);
//       } else {
//         reject("Error fetching User ");
//       }
//     }, 3000);
//   });
// }
// async function fetchPosts() {
//   try {
//     const res = await makeAPIResquest();
//     console.log(res);
//   } catch (err) {
//     console.log(err);
//   }
// }
// fetchPosts();
